const rover = require("./app");

test("default rover direction is north", () => {
  expect(rover.direction).toBe("n");
});

test("when rover direction is north and it turns left 3 times direction should be east", () => {
  rover.turnLeft();
  rover.turnLeft();
  rover.turnLeft();
  expect(rover.direction).toBe("e");
});

test("when rover direction is east and it turns right direction should be south", () => {
  rover.turnRight();
  expect(rover.direction).toBe("s");
});

test("the default position of the rover is 0,0", () => {
  expect(rover.position).toEqual({ x: 0, y: 0 });
});

test("If the Rover is facing west and moves forward, its positionX should decrease by 1", () => {
  rover.direction = "w";
  rover.moveForward();
  expect(rover.position).toEqual({ x: -1, y: 0 });
});

test("If the rover is facing north and moves forward, we would decrease the rover’s y by 1.", () => {
  rover.direction = "n";
  rover.moveForward();
  expect(rover.position).toEqual({ x: -1, y: -1 });
});

test("If the rover is facing south and moves forward, we would increase the y by 1.", () => {
  rover.direction = "s";
  rover.moveForward();
  expect(rover.position).toEqual({ x: -1, y: 0 });
});

test('When the rover gets the commands "rffrf" the rovers position should be ', () => {
  rover.direction = "n";
  rover.position = { x: 0, y: 0 };
  rover.commands("rffrf");
  expect(rover.position).toEqual({ x: 2, y: 1 });
});

test('When the rover gets the commands "rffrf" the rovers position should be ', () => {
  rover.direction = "n";
  rover.travelLog = [[0, 0]];
  rover.position = { x: 0, y: 0 };
  rover.commands("rffrf");
  expect(rover.travelLog).toEqual([[0, 0], [1, 0], [2, 0], [2, 1]]);
});

test("The rover should be able to move backwards", () => {
  rover.direction = "n";
  rover.position = { x: 0, y: 0 };
  rover.moveBackward();
  expect(rover.position).toEqual({ x: 0, y: 1 });
});

test("The commands should be valid, when a command is not valid rover ignores them", () => {
  rover.direction = "n";
  rover.travelLog = [[0, 0]];
  rover.position = { x: 0, y: 0 };
  rover.commands("ffzzy");
  expect(rover.position).toEqual({ x: 0, y: -2 });
});