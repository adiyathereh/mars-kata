const rover = {
  initRoverPosition: function() {
    this.position = this.getRoverPosition();
  },
  direction: "n",
  field: [],
  travelLog: [],
  getRoverPosition: function() {
    if (this.field.length > 0) {
      for (let i = 0; i < this.field.length; i++) {
        if (this.field[i].includes("x") === true) {
          this.position.x = 1;
          this.position.y = this.field[i].indexOf("x");
          console.log(rover.position);
        }
      }
    } else {
        return { x: 0, y: 0 };
    }
  },
  moveForward: function() {
    if (this.direction === "n") {
      this.position["y"]--;
    } else if (this.direction === "e") {
      this.position["x"]++;
    } else if (this.direction === "s") {
      this.position["y"]++;
    } else if (this.direction === "w") {
      this.position["x"]--;
    }
    this.travelLog.push([this.position.x, this.position.y]);
  },
  moveBackward: function() {
    if (this.direction === "n") {
      this.position["y"]++;
    } else if (this.direction === "e") {
      this.position["x"]--;
    } else if (this.direction === "s") {
      this.position["y"]--;
    } else if (this.direction === "w") {
      this.position["x"]++;
    }
    this.travelLog.push([this.position.x, this.position.y]);
  },
  turnLeft: function() {
    switch (this.direction) {
      case "n":
        this.direction = "w";
        break;
      case "e":
        this.direction = "n";
        break;
      case "s":
        this.direction = "e";
        break;
      case "w":
        this.direction = "s";
        break;
    }
  },
  turnRight: function() {
    switch (this.direction) {
      case "n":
        this.direction = "e";
        break;
      case "e":
        this.direction = "s";
        break;
      case "s":
        this.direction = "w";
        break;
      case "w":
        this.direction = "n";
        break;
    }
  },
  commands: function(directions) {
    for (let i = 0; i < directions.length; i++) {
      if (directions[i] === "r") {
        this.turnRight();
      } else if (directions[i] === "l") {
        this.turnLeft();
      } else if (directions[i] === "f") {
        this.moveForward();
      } else if (directions[i] === "b") {
        this.moveBackward();
      }
    }
  }
};
rover.initRoverPosition();

module.exports = rover;
